<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

require_once('vendor/autoload.php');

// Create client
$client = new Client();
// Add two params
$url = '<LINK-TO-FILE>';
$token = '<TOKEN>';
//link to api model and options params
$link_to_api = 'https://api.deepgram.com/v1/listen?tier=base&version=latest&language=ru&punctuate=true&utterances=true';

try {
    $response = $client->request('POST', $link_to_api, [
        'body' => sprintf('{"url":"%s"}', $url),
        'headers' => [
            'Authorization' => sprintf('Token %s', $token),
            'accept' => 'application/json',
            'content-type' => 'application/json',
        ],
    ]);
} catch (GuzzleException $e) {
    print_r($e);
}

// request decode to array
$answer = json_decode($response->getBody(), true);
// get output text
echo $answer["results"]["channels"][0]["alternatives"][0]["transcript"];
