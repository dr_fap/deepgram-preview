# Deepgram Preview

![Deephram preview](https://deepgram.com/wp-content/uploads/2023/04/open-graph-deepgram-speech-to-text.png)

## Начало

Сначала нужно пройти регистрацию на этом сайте: [**Deepgram**](https://deepgram.com/)


## Склонировать репозиторий

```
git clone https://gitlab.com/dr_fap/deepgram-preview.git
```

## Указать данные

- `$token` - API-token от сервиса. Можно получить здесь: [Create a New API Key](https://console.deepgram.com/project/)
- `$url` - Ссылка на Ваш оригинальный файл в следующих форматах:

  | MIME type                                                  | Audio or video type                                                                                                                                                                     |
  |------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
  | `audio/wave`, `audio/wav`, `audio/x-wav`, `audio/x-pn-wav` | An audio file in the WAVE container format. The PCM audio codec (WAVE codec "1") is often supported, but other codecs have limited support (if any).                                    |
  | `audio/webm`                                               | An audio file in the WebM container format. Vorbis and Opus are the codecs officially supported by the WebM specification.                                                              |
  | `video/webm`                                               | A video file, possibly with audio, in the WebM container format. VP8 and VP9 are the most common video codecs; Vorbis and Opus the most common audio codecs.                            |
  | `audio/ogg`                                                | An audio file in the Ogg container format. Vorbis is the most common audio codec used in such a container; however, Opus is now supported by Ogg as well.                               |
  | `video/ogg`                                                | A video file, possibly with audio, in the Ogg container format. Theora is the usual video codec used within it; Vorbis is the usual audio codec, although Opus is becoming more common. |
  | `application/ogg`                                          | An audio or video file using the Ogg container format. Theora is the usual video codec used within it; Vorbis is the usual audio codec.                                                 |
- `$link_to_api` - Ссылка на модель внутри API. Также можно указать дополнительные параметры. ([СМ ЗДЕСЬ](https://developers.deepgram.com/reference/pre-recorded))

## Запустить